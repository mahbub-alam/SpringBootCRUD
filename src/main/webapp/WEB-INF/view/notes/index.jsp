<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Note List</h2>
  <p>The .table-bordered class adds borders on all sides of the table and the cells:</p>            
  ${notes[0].id}
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Sl</th>
        <th>Id</th>
        <th>Title</th>
        <th>Created At</th>
        <th>Updated At</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <c:foreach items="${notes}" var="note">
    	<tr>
    	<td></td>
        <td>${note.id}</td>
        <td>${note.title}</td>
        <td>${note.created_at}</td>
        <td>${note.updated_at}</td>
        <td>Edit</td>
      </tr>
    </c:foreach>
      
    </tbody>
  </table>
</div>

</body>
</html>
